import { PlatziPracticaDevopsPage } from './app.po';

describe('platzi-practica-devops App', function() {
  let page: PlatziPracticaDevopsPage;

  beforeEach(() => {
    page = new PlatziPracticaDevopsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
